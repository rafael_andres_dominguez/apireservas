'use strict';

/**
 * @ngdoc overview
 * @name apiReservasApp
 * @description
 * # apiReservasApp
 *
 * Main module of the application.
 */
angular.module('apiReservasApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'objectTable',
    'LocalStorageModule'
])
        .config(['localStorageServiceProvider', function (localStorageServiceProvider) {
                localStorageServiceProvider.setPrefix('ls')
                        .setStorageType('sessionStorage');
            }])
        .config(function ($routeProvider, $locationProvider) {
            $routeProvider
                    .when('/usuarios', {
                        controller: 'HomeController',
                        templateUrl: 'views/home.html',
                        controllerAs: 'vm'
                    })
                    .when('/login', {
                        controller: 'LoginController',
                        templateUrl: 'views/login.html',
                        controllerAs: 'vm'
                    })
                    .when('/register', {
                        controller: 'UserController',
                        templateUrl: 'views/register.html',
                        controllerAs: 'vm'
                    })
                    .when('/register-user', {
                        templateUrl: 'views/registerUser.html'
                    })
                    .when('/vuelos', {
                        templateUrl: 'views/consultaVuelo.html'
                    })
                    .when('/reserva', {
                        templateUrl: 'views/consultaReserva.html'
                    })
                    .otherwise({redirectTo: '/login'});
        })
        .run(function ($rootScope, $location, $cookies, $http, AuthenticationService, localStorageService) {
            // keep user logged in after page refresh
            $rootScope.globals = $cookies.getObject('globals') || {};
            if ($rootScope.globals.currentUser) {
                $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata;
            }
            if (localStorageService.get("user") === null) {
                $location.path("/login");
                return;
            }
            $rootScope.$on('$locationChangeStart', function (event, next, current) {
                // redirect to login page if not logged in and trying to access a restricted page
                var restrictedPage = $.inArray($location.path(), ['/login', '/register']) === -1;
                var loggedIn = $rootScope.globals.currentUser;
                if (restrictedPage && !loggedIn) {
                    $location.path('/login');
                }
            });
        }
        );