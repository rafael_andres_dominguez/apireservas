(function () {
    'use strict';
    angular.module('apiReservasApp').controller('ReservaController', ReservaController);
 
    ReservaController.$inject = ['ReservaService', '$location', '$rootScope', 'FlashService','localStorageService'];
    function ReservaController(ReservaService, $location, $rootScope, FlashService,localStorageService) {
        var vm = this; 
        vm.allReservasUsers = [];

        initController();

        function initController() {
            allReservasUsers();
        }

        function allReservasUsers() {
            var userRol = localStorageService.get('user');
            ReservaService.GetByIdUser(userRol.id).then(function (data) {
                vm.allReservasUsers = data.data.responseList;
            });
        }
    }
 
})();

