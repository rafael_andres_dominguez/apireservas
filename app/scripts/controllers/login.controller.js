'use strict';
(function () { 
    angular.module('apiReservasApp').controller('LoginController', LoginController);
 
    LoginController.$inject = ['$scope', '$location', 'AuthenticationService', 'FlashService','UserService', '$rootScope','localStorageService'];
    function LoginController($scope, $location, AuthenticationService, FlashService,UserService, $rootScope,localStorageService) {
        var loginCtrl = this;
        loginCtrl.usuario = AuthenticationService.user;
        loginCtrl.login = login;
        
        (function initController() {
            // reset login status
            AuthenticationService.ClearCredentials();
            loginCtrl.usuario.userName='';
//            localStorageService.clearAll();
        })();
 
        function login() {            
            loginCtrl.dataLoading = true;
            AuthenticationService.Login(loginCtrl.userName, loginCtrl.password, function (response) {
                if (response.success) {
                    AuthenticationService.SetCredentials(loginCtrl.userName, loginCtrl.password);
                    //update reference user in sessión
                    UserService.GetByUsername(loginCtrl.userName).then(function (user) {
                        loginCtrl.usuario.userName=user.data.userName;
                        loginCtrl.usuario.firstName=user.data.firstName;
                        loginCtrl.usuario.lastName=user.data.lastName;
                        localStorageService.set("user", user.data);
                    });
                    $location.path('/vuelos');
                } else {
                    FlashService.Error(response.message);
                    loginCtrl.dataLoading = false;
                }
            });
        };
        
        
        loginCtrl.logout = function () {
            $location.path("/login");
            localStorageService.clearAll();
        };
    }
        
})();


