(function () {
    'use strict';

    angular.module('apiReservasApp').controller('HomeController', HomeController);

    HomeController.$inject = ['UserService', '$rootScope','$location','FlashService'];
    function HomeController(UserService, $rootScope,$location,FlashService) {
        var vm = this;
        
        vm.allUsers = [];
        vm.userVisor= {};
        vm.userEntity = UserService.usuario;
        vm.deleteUser = deleteUser;
        vm.register = register;
        vm.usuario = null;
        vm.listaEstados = [{estado:"Habilitado"},{estado:"Inactivo"}];
        initController();

        function initController() {
            loadAllUsers();
        }

        function loadAllUsers() {
            UserService.GetAll().then(function (users) {
                vm.allUsers = users;
            });
        }

        function deleteUser(id) {
            vm.dataLoading = true;
            UserService.Delete(id).then(function () {
                FlashService.Success('Eliminación exitosa!', true);
                loadAllUsers();
            });
            vm.dataLoading = false;
        }

        vm.onClickToEditar = function (item) {
            vm.userVisor.onDeshabilitar = false;
            vm.userVisor.onOcultar = false;
            vm.userEntity.id = item.id;
            vm.userEntity.firstName = item.firstName;
            vm.userEntity.lastName = item.lastName;
            vm.userEntity.userName = item.userName;
            vm.userEntity.password = item.password;
            vm.userEntity.status = item.status;
            $location.path('/register-user');
        };
        
        vm.onLimpiarRegistro = function () {
            vm.userVisor.onDeshabilitar = false;
            vm.userVisor.onOcultar = false;
            vm.userEntity.id = null;
            vm.userEntity.firstName = '';
            vm.userEntity.lastName = '';
            vm.userEntity.userName = '';
            vm.userEntity.password = '';
            vm.userEntity.status = '';
        };
        
        function register() {
            vm.dataLoading = true;
            if(vm.userEntity.id === null){
                UserService.GetByUsername(vm.userEntity.userName).then(function (response){
                    if (response.data !== null && response.data !== "") {
                        FlashService.Error('Usuario ya existe!', true);
                        vm.dataLoading = false;
                    } else {
                        UserService.Create(vm.userEntity)
                            .then(function (response) {
                                if (response.success) {
                                    FlashService.Success('Registro exitoso!', true);
                                    $location.path('/usuarios');
                                } else {
                                    FlashService.Error(response.message);
                                    vm.dataLoading = false;
                                }
                            });
                    }
                });
            }else{//update
                UserService.GetByUsername(vm.userEntity.userName).then(function (response){
                    if (response.data !== null && response.data !== "" && response.data.id !== vm.userEntity.id) {
                        FlashService.Error('Usuario ya existe!', true);
                        vm.dataLoading = false;
                    }else{
                        UserService.Update(vm.userEntity)
                            .then(function (response) {
                                if (response.success) {
                                    FlashService.Success('Actualización exitosa!', true);
                                    $location.path('/usuarios');
                                } else {
                                    FlashService.Error(response.message);
                                    vm.dataLoading = false;
                                }
                            });
                    }
                });
            }
            loadAllUsers();
        }
    }

})();