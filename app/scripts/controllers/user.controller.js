(function () {
    'use strict';
 
    angular.module('apiReservasApp').controller('UserController', UserController);
 
    UserController.$inject = ['UserService', '$location', '$rootScope', 'FlashService'];
    function UserController(UserService, $location, $rootScope, FlashService) {
        var vm = this; 
        vm.register = register;
 
        function register() {
            vm.dataLoading = true;
            UserService.GetByUsername(vm.user.userName).then(function (response){
                if (response.data !== null && response.data !== "") {
                    FlashService.Error('Usuario ya existe!', true);
                    vm.dataLoading = false;
                } else {
                    UserService.Create(vm.user)
                        .then(function (response) {
                            if (response.success) {
                                FlashService.Success('Registro exitoso!', true);
                                $location.path('/login');
                            } else {
                                FlashService.Error(response.message);
                                vm.dataLoading = false;
                            }
                        });
                }
            }); 
        }
    }

})();

