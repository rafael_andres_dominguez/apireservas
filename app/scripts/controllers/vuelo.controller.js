(function () {
    'use strict';
    angular.module('apiReservasApp').controller('VueloController', VueloController);

    VueloController.$inject = ['VueloService', 'ReservaService', '$location', '$rootScope', 'FlashService', 'localStorageService'];
    function VueloController(VueloService, ReservaService, $location, $rootScope, FlashService, localStorageService) {
        var ctrlVuelo = this;
        ctrlVuelo.listAeropuertos = [];
        ctrlVuelo.listVuelosSalidas = [];
        ctrlVuelo.listVuelosLlegadas = [];
        ctrlVuelo.validationCheck = true;
        
        ctrlVuelo.busquedaDTO = {
            destino: null,
            origen: null,
            salida: null,
            llegada: null,
            pasajeros: 1
        };
        
        ctrlVuelo.onConsultar = onConsultar;
        ctrlVuelo.onReservar = onReservar;
        ctrlVuelo.checkvalidation = checkvalidation;
        
        function inicializarVuelos() {
            VueloService.GetAllAeropuertos().then(function (response) {
                ctrlVuelo.listAeropuertos = response;
            });
        }

        function onReservar(item) {
            var userRol = localStorageService.get('user');
            
            //Se valida edad del usuario en sesión
            if(userRol.edad < 18){
                FlashService.Error('Sr. Usuario, para realizar reservas debe ser mayor de edad.');
                return;
            }
            //Cantidad de pasajeros debe ser mayor a cero.
            if(ctrlVuelo.busquedaDTO.pasajeros < 1){
                FlashService.Error('La cantidad de pasajeros debe ser mayor a cero(0).');
                return;
            }
            //Cantidad de pasajeros debe ser mayor o igual a la disponibilidad del vuelo.
            if(ctrlVuelo.busquedaDTO.pasajeros < item.disponibilidad){
                FlashService.Error('La cantidad de pasajeros debe ser mayor o igual a la disponibilidad del vuelo '+item.vuelo+'.');
                return;
            }
                        
            var reservaDTO = {
                codeReserva: userRol.id +"-"+ item.vuelo,
                codeVuelo: item.id,
                fechaRegistro: new Date(),
                idUser: userRol.id,
                status: "RESERVADO",
                pasajeros: ctrlVuelo.busquedaDTO.pasajeros
            };
            
            ReservaService.Create(reservaDTO).then(function (response) {
                if (response.success) {
                    if (response.data.tipo === 400) {
                        FlashService.Error(response.data.message);
                        $location.path('/vuelos');
                    } else {
                        FlashService.Success(response.data.message, true);
                        $location.path('/vuelos');
                    }
                } else {
                    FlashService.Error(response.message);
                }
            });
            onConsultar();
        }

        function onConsultar() {
            FlashService.initService();
            if(ctrlVuelo.busquedaDTO.origen === null){
                FlashService.Error("Seleccionar Origen.");
                return;
            }
            if(ctrlVuelo.busquedaDTO.destino === null){
                FlashService.Error("Seleccionar Destino.");
                return;
            }
            if(ctrlVuelo.busquedaDTO.salida === null){
                FlashService.Error("Seleccionar fecha de salida.");
                return;
            }
            if(ctrlVuelo.busquedaDTO.llegada === null && (ctrlVuelo.chkselct === false || ctrlVuelo.chkselct === undefined)){
                FlashService.Error("Seleccionar fecha de llegada.");
                return;
            }
            if(ctrlVuelo.busquedaDTO.pasajeros <= 0){
                FlashService.Error("La cantidad de pasajeros debe ser mayor a cero (0).");
                return;
            }
            VueloService.GetVuelosByFiltroSalida(ctrlVuelo.busquedaDTO).then(function (response) {
                if (response.success) {
                    ctrlVuelo.listVuelosSalidas = response.data.objectResponse;
                } else {
                    ctrlVuelo.listVuelosSalidas = [];
                    FlashService.Error("No hay vuelos de salida en la fecha seleccionada.");
                }
            });
            if(ctrlVuelo.chkselct === false){
                VueloService.GetVuelosByFiltroLlegada(ctrlVuelo.busquedaDTO).then(function (response) {
                    if (response.success) {
                        ctrlVuelo.listVuelosLlegadas = response.data.objectResponse;
                    } else {
                        ctrlVuelo.listVuelosLlegadas = [];
                        FlashService.Error("No hay vuelos de llegada en la fecha seleccionada.");
                    }
                });
            }
        }

        function checkvalidation() {
            var chkselct = ctrlVuelo.chkselct;
            if (chkselct === false || chkselct === undefined){
                ctrlVuelo.validationCheck = true;
            }else{
                ctrlVuelo.validationCheck = false;
            }
        };
        
        inicializarVuelos();
    }
})();

