(function () {
    'use strict';
    angular.module('apiReservasApp').factory('UserService', UserService);
 
    UserService.$inject = ['$http'];
    function UserService($http) {
        var service = {}; 
        service.GetAll = GetAll;
        service.GetById = GetById;
        service.GetByUsername = GetByUsername;
        service.Create = Create;
        service.Update = Update;
        service.Delete = Delete;
        service.usuario={};
        return service;
 
        function GetAll() {
            return $http.get('http://localhost:8085/users').then(handleSuccessList, handleError('Error consultando todos los usuarios'));
        }
 
        function GetById(id) {
            return $http.get('http://localhost:8085/users/' + id).then(handleSuccess, handleError('Error consultando usuarios by id'));
        }
 
        function GetByUsername(userName) {
            return $http.get('http://localhost:8085/users/byUserName/' + userName).then(handleSuccess, handleError('Error consultando usuario por User Name'));
        }
 
        function Create(user) {
            return $http.post('http://localhost:8085/users', user).then(handleSuccessPost, 
            handleError('Error creando usuario'));
        }
 
        function Update(user) {
            return $http.put('http://localhost:8085/users/' + user.id, user).then(handleSuccess, handleError('Error actualizando usuario'));
        }
 
        function Delete(id) {
            return $http.delete('http://localhost:8085/users/' + id.toString()).then(handleSuccess, handleError('Error eliminando usuario'));
        }
 
        // private functions 
        function handleSuccess(res) {
            return {success: true , data: res.data};
        }
        
        function handleSuccessPost(res) {
            return {success: true};
        }

        function handleSuccessList(res) {
            return res.data.responseList;
        }
 
        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }
    }
})();