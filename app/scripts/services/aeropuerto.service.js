(function () {
    'use strict';
    angular.module('apiReservasApp').factory('AeropuertoService', AeropuertoService);
 
    AeropuertoService.$inject = ['$http'];
    function AeropuertoService($http) {
        var service = {}; 
        service.GetAll = GetAll;
        service.GetById = GetById;
        service.Create = Create;
        service.aeropuerto={};
        return service;
 
        function GetAll() {
            return $http.get('http://localhost:8085/aeropuertos').then(handleSuccessList, handleError('Error consultando todos los aeropuertos'));
        }
 
        function GetById(id) {
            return $http.get('http://localhost:8085/aeropuertos/' + id).then(handleSuccess, handleError('Error consultando aeropuertos by id'));
        }
 
        function Create(aeropuerto) {
            return $http.post('http://localhost:8085/aeropuertos', aeropuerto).then(handleSuccessPost,handleError('Error creando aeropuerto'));
        }
  
        // private functions 
        function handleSuccess(res) {
            return {success: true , data: res.data};
        }
        
        function handleSuccessPost(res) {
            return {success: true};
        }

        function handleSuccessList(res) {
            return res.data.responseList;
        }
 
        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }
    }
})();