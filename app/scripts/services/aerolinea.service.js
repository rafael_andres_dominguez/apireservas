(function () {
    'use strict';
    angular.module('apiReservasApp').factory('AerolineaService', AerolineaService);
 
    AerolineaService.$inject = ['$http'];
    function AerolineaService($http) {
        var service = {}; 
        service.GetAll = GetAll;
        service.GetById = GetById;
        service.Create = Create;
        service.aerolinea={};
        return service;
 
        function GetAll() {
            return $http.get('http://localhost:8085/aerolineas').then(handleSuccessList, handleError('Error consultando todos los aerolineas'));
        }
 
        function GetById(id) {
            return $http.get('http://localhost:8085/aerolineas/' + id).then(handleSuccess, handleError('Error consultando aerolineas by id'));
        }
 
        function Create(aerolinea) {
            return $http.post('http://localhost:8085/aerolineas', aerolinea).then(handleSuccessPost,handleError('Error creando aerolinea'));
        }
  
        // private functions 
        function handleSuccess(res) {
            return {success: true , data: res.data};
        }
        
        function handleSuccessPost(res) {
            return {success: true};
        }

        function handleSuccessList(res) {
            return res.data.responseList;
        }
 
        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }
    }
})();