(function () {
    'use strict';
    angular.module('apiReservasApp').factory('ReservaService', ReservaService);

    ReservaService.$inject = ['$http'];
    function ReservaService($http) {
        var service = {};
        service.GetAll = GetAll;
        service.GetById = GetById;
        service.GetByIdUser = GetByIdUser;
        service.Create = Create;
        service.reserva = {};
        return service;

        function GetAll() {
            return $http.get('http://localhost:8085/reservas').then(handleSuccessList, handleError('Error consultando todos los reservas'));
        }

        function GetById(id) {
            return $http.get('http://localhost:8085/reservas/' + id).then(handleSuccess, handleError('Error consultando reservas by id'));
        }
        
        function GetByIdUser(idUser) {
            return $http.get('http://localhost:8085/reservas/byIdUser/' + idUser).then(handleSuccess, handleError('Error consultando reservas by idUser'));
        }

        function Create(reserva) {
            return $http.post('http://localhost:8085/reservas', reserva).then(handleSuccessPost, handleError('Error creando reserva'));
        }

        // private functions
        function handleSuccess(res) {
            return {success: true, data: res.data};
        }

        function handleSuccessPost(res) {
            return {success: true, data: res.data};
        }

        function handleSuccessList(res) {
            return res.data.responseList;
        }

        function handleError(error) {
            return function () {
                return {success: false, message: error};
            };
        }
    }
})();