(function () {
    'use strict';
    angular.module('apiReservasApp').factory('VueloService', VueloService);

    VueloService.$inject = ['$http'];
    function VueloService($http) {
        var service = {};
        service.GetAll = GetAll;
        service.GetAllAeropuertos = GetAllAeropuertos;
        service.GetVuelosByFiltroSalida = GetVuelosByFiltroSalida;
        service.GetVuelosByFiltroLlegada = GetVuelosByFiltroLlegada;
        service.GetById = GetById;
        service.Create = Create;
        service.vuelo = {};
        return service;

        function GetAll() {
            return $http.get('http://localhost:8085/vuelos').then(handleSuccessList, handleError('Error consultando todos los vuelos'));
        }
        function GetAllAeropuertos() {
            return $http.get('http://localhost:8085/aeropuertos').then(handleSuccessList, handleError('Error consultando todos los aeropuertos'));
        }

        function GetVuelosByFiltroSalida(busquedaDTO) {
            return $http.get('http://localhost:8085/vuelos/findOrigen/' + busquedaDTO.origen.id + '/' + busquedaDTO.destino.id + '/' + busquedaDTO.salida)
                    .then(handleSuccess, handleError('Error consultando vuelos llegadas'));
        }

        function GetVuelosByFiltroLlegada(busquedaDTO) {
            return $http.get('http://localhost:8085/vuelos/findDestino/' + busquedaDTO.destino.id + '/' + busquedaDTO.origen.id + '/' + busquedaDTO.llegada)
                    .then(handleSuccess, handleError('Error consultando vuelos destino'));
        }

        function GetById(id) {
            return $http.get('http://localhost:8085/vuelos/' + id).then(handleSuccess, handleError('Error consultando vuelos by id'));
        }

        function Create(user) {
            return $http.post('http://localhost:8085/vuelos', user).then(handleSuccessPost, handleError('Error creando vuelo'));
        }

        // private functions
        function handleSuccess(res) {
            return {success: true, data: res.data};
        }

        function handleSuccessPost(res) {
            return {success: true};
        }

        function handleSuccessList(res) {
            return res.data.responseList;
        }

        function handleError(error) {
            return function () {
                return {success: false, message: error};
            };
        }
    }
})();